#!/bin/bash

run_member() {
	[ -z "${MEMBER_ID}" ] && exit 1
	echo "Starting etcd-${MEMBER_ID}"
	if [ -d "/tmp/etcd-${MEMBER_ID}" ]; then
		rm -rfv /tmp/etcd-${MEMBER_ID}
	fi
	etcd --name etcd-${MEMBER_ID} --data-dir /tmp/etcd-${MEMBER_ID}/ --listen-peer-urls=https://127.0.0.1:2380${MEMBER_ID},https://[::1]:2380${MEMBER_ID} --listen-client-urls=https://127.0.0.1:2379${MEMBER_ID},https://[::1]:2379${MEMBER_ID} --initial-advertise-peer-urls=https://localhost:2380${MEMBER_ID} --initial-cluster=etcd-0=https://localhost:23800,etcd-1=https://localhost:23801,etcd-2=https://localhost:23802 --advertise-client-urls=https://localhost:2379${MEMBER_ID} --auto-tls --peer-auto-tls --log-level=debug > /tmp/log-etcd-${MEMBER_ID} 2>&1
	exit 0
}

MEMBER_ID=0 run_member &
MEMBER_ID=1 run_member &
MEMBER_ID=2 run_member &


EX="export ETCDCTL_ENDPOINTS=https://localhost:23790,https://localhost:23791,https://localhost:23792
exportETCDCTL_INSECURE_SKIP_TLS_VERIFY=true"
eval ${EX}

# wait for cluster to be healthy
export ETCDCTL_INSECURE_SKIP_TLS_VERIFY=true
while ! etcdctl member list; do sleep 1; done

echo "All members are up"
echo $EX

etcdctl member list
etcdctl endpoint status --write-out=table

echo "Executing new shell with working etcdctl"
PS1="etcdshell $PS1" bash
killall etcd

wait
echo "Etcd servers terminated"
