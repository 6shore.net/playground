package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.etcd.io/etcd/clientv3"
	"go.etcd.io/etcd/pkg/transport"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

func main() {
	defer log.Println("Test finished")

	ep := []string{}
	// generate endpoints
	for i := 23790; i <= 23792; i++ {
		u := fmt.Sprintf("https://localhost:%d", i)
		ep = append(ep, u)
	}

	log.Printf("Using endpoints %s", ep)
	ctx := context.Background()

	tlsInfo := transport.TLSInfo{
		InsecureSkipVerify: true,
	}
	tlsConfig, err := tlsInfo.ClientConfig()
	if err != nil {
		log.Fatal(err)
	}

	logConfig := zap.NewProductionConfig()
	logConfig.Level = zap.NewAtomicLevelAt(zap.DebugLevel)

	config := clientv3.Config{
		Endpoints:   ep,
		DialTimeout: 1 * time.Second,
		// etcd client will hang until ctx closes (= forever) without this option
		DialOptions: []grpc.DialOption{grpc.WithBlock()},
		Context:     ctx,
		TLS:         tlsConfig,
		LogConfig:   &logConfig,
	}

	cl, err := clientv3.New(config)
	if err != nil {
		log.Fatalf("Creating client: %s", err)
	}
	defer cl.Close()

	// list members
	log.Println("Listing members")
	memberList, err := cl.MemberList(ctx)
	if err != nil {
		log.Fatalf("Listing members: %s", err)
	}
	for _, m := range memberList.Members {
		log.Printf("Found member %+v", m)
	}
}
